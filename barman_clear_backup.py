# https://gitlab.com/arvaroh_scripts/barman_backup_rotation_script
import os
import re
import shutil
import logging
import datetime


class BackupCleaner(object):
    SERVER_LIST = [
            'server1.local',
            'server2.local',
            'server3.local',
            'server4.local'
        ]
    BARMAN_PATH = '/barman/'

    def __init__(self):
        logging.basicConfig(filename='/var/log/barman/barman.log', level=logging.DEBUG,
                            format='[%(asctime)s] %(message)s', datefmt='%Y-%m-%d %I:%M:%S %p')
        print("Starting [BARMAN_CLEAR_BACKUP]...")
        logging.info('[BARMAN_CLEAR_BACKUP] Starting...')

    @staticmethod
    def backup_info(directory, backup):
        """
        Parse backup info into dict.
        :return:
        """
        info = {}
        f = os.popen("barman show-backup " + directory + " " + backup)
        for line in f.readlines():
            if not re.match(r'^\s*$', line):
                data = line.strip().split(': ')
                if len(data) == 2:
                    info[data[0].rstrip()] = data[1]
        return info

    @staticmethod
    def check_backup_date(backup_info, status):
        """
        Check backup date. If backup older than 3 days -> DELETE
                           If backup is not incremental (Full backup)
                        and this is backup of first day of the week or first day of the month -> SAVE
        :param backup_info: Backup information (dict)
        :param status: Backup status/type
        :return:
        """
        date = datetime.datetime.strptime(backup_info['End time'], '%Y-%m-%d %H:%M:%S.%fZ')
        if status == 'FULL':
            if date.weekday() == 0 or date.day == 1:
                return False
        if date.date() <= (datetime.datetime.now() - datetime.timedelta(days=3)).date():
            return True
        return False

    @staticmethod
    def check_backup_status(backup_info):
        """
        Check Backup Status.
        [INCREMENTAL/FAILED/FULL]
        :param backup_info: Backup information (dict)
        :return: Backup status
        """
        if backup_info['status'] == 'FAILED':
            return backup_info['status']
        if '-0.00%' in backup_info['Incremental size']:
            return 'FULL'
        return 'INCREMENTAL'

    def run(self):
        """
        Main loop.
        If backup status FAILED or backup older than 3 days -> REMOVE.
        :return:
        """
        for directory in os.listdir(self.BARMAN_PATH):
            if directory in self.SERVER_LIST:
                path = self.BARMAN_PATH + directory + '/base/'
                for backup in os.listdir(path):
                    backup_info = self.backup_info(directory, backup)
                    backup_status = self.check_backup_status(backup_info)
                    if self.check_backup_date(backup_info, backup_status) or backup_status == 'FAILED':
                        print("Removing %s: %s backup" % (directory, backup))
                        logging.debug("Removing %s: %s backup" % (directory, backup))
                        shutil.rmtree(path + backup, ignore_errors=True)
        print("[BARMAN_CLEAR_BACKUP] Finished...")
        logging.info('[BARMAN_CLEAR_BACKUP] Finished...')


cleaner = BackupCleaner()
if __name__ == '__main__':
    try:
        cleaner.run()
    except Exception as e:
        print(e)
        logging.exception('[BARMAN_CLEAR_BACKUP] Error: {}'.format(e))
